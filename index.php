<?php
	defined('_JEXEC') or die;
	$document = JFactory::getDocument();
	$app  = JFactory::getApplication();
	$user = JFactory::getUser();
	$menu = $app->getMenu();
	$lang = JFactory::getLanguage();
	// Detecting Active Variables
	$option   = $app->input->getCmd('option', '');
	$view     = $app->input->getCmd('view', '');
	$layout   = $app->input->getCmd('layout', '');
	$task     = $app->input->getCmd('task', '');
	$itemid   = $app->input->getCmd('Itemid', '');
	$user_status = $user->guest;
	// set class for content
	$infoRightCountModule = $this->countModules('info-right');
	if ($infoRightCountModule)
	{
		$col = 'col-sm-8';
	}
	else
	{
		$col = 'col-sm-12';
	}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">    
    <jdoc:include type="head"/>
	<!-- Bootstrap Core CSS -->
	<?php 
		$document->addStyleSheet('templates/' . $this->template .'/css/bootstrap.min.css');
		$document->addStyleSheet('templates/' . $this->template .'/css/font-awesome/css/font-awesome.min.css');
		if($option == "com_users" && $view != "profile"){
			$document->addStyleSheet('templates/' . $this->template .'/css/style_login.css');
		}
		else{
			$document->addStyleSheet('templates/' . $this->template .'/css/style.css');
		}
	?>
</head>
<body>
    <div class="container">
		<?php if($option == "com_users" && $view != "profile") { ?>
			<!-- Content for component -->						
			<jdoc:include type="message" />
			<jdoc:include type="component" />
		<?php } else { ?>
			<?php if ($this->countModules('logo') || $this->countModules('right-banner')) : ?>
				<!-- top logo and banner -->
				<div class="row justify-content-between">
					<?php if ($this->countModules('logo')) : ?>
						<div class="col-3">
							<jdoc:include type="modules" name="logo" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('right-banner')) : ?>
						<div class="col-4">
							<!-- right banner -->
							<jdoc:include type="modules" name="right-banner" style="none"/>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<!-- menu -->
			<?php if ($this->countModules('menu')) : ?>
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">Logo</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNavDropdown">
						<jdoc:include type="modules" name="menu" style="none"/>				
						<ul class="nav navbar-nav my-2 my-lg-0">
							<?php if ($user_status) { ?>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>">Login</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">Registration</a>
								</li>
							<?php } else { ?>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_users&view=login&layout=logout&task=user.menulogout'); ?>">Logout</a>
								</li>
							<?php } ?>
						</ul>				
					</div>
				</nav>
			<?php endif; ?>

			<?php if ($menu->getActive() != $menu->getDefault($lang->getTag())) { ?>		
				<?php if ($this->countModules('top-banner')) : ?>
					<!-- Header with Background Image -->
					<header>
						<jdoc:include type="modules" name="top-banner" style="none"/>			
					</header>
				<?php endif; ?>
			<?php } else {?>
				<?php if ($this->countModules('big-sliders')) : ?>
					<header>
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<jdoc:include type="modules" name="big-sliders" style="none"/>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>						
					</header>			
				<?php endif; ?>
			<?php } ?>
			<?php if ($menu->getActive() != $menu->getDefault($lang->getTag())) { ?>
				<div id="main-content">
					<div class="row">
						<div class="<?php echo $col; ?>">
							<!-- Content for component -->						
							<jdoc:include type="message" />
							<jdoc:include type="component" />
						</div>
						<?php if ($infoRightCountModule) {?>
							<div class="col-sm-4 my-4">
								<jdoc:include type="modules" name="info-right" style="none"/>
							</div>
						<?php }?>
					</div>
				</div>
			<?php } else {?>
				<div class="row">
					<?php if ($this->countModules('info-1')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="info-1" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('info-2')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="info-2" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('info-3')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="info-3" style="none"/>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-center">
					<div class="col-sm-10">
						<div class="row">
							<?php if ($this->countModules('content-1')) : ?>
								<div class="col my-4">
									<jdoc:include type="modules" name="content-1" style="none"/>
								</div>
							<?php endif; ?>
							<?php if ($this->countModules('content-2')) : ?>
								<div class="col my-4">
									<jdoc:include type="modules" name="content-2" style="none"/>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php } ?>
			<div id="footer-content">
				<?php if ($this->countModules('content-3')) : ?>
					<div class="row">
						<div class="col-sm-12 my-4">						
							<jdoc:include type="modules" name="content-3" style="none"/>
						</div>
					</div>
				<?php endif; ?>
				<?php if ($this->countModules('content-4') || $this->countModules('content-5')) : ?>
					<div class="row">
						<?php if ($this->countModules('content-4')) : ?>
							<div class="col my-4">
								<jdoc:include type="modules" name="content-4" style="none"/>
							</div>
						<?php endif; ?>
						<?php if ($this->countModules('content-5')) : ?>
							<div class="col my-4">
								<jdoc:include type="modules" name="content-5" style="none"/>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if ($this->countModules('content-6')) : ?>
					<div class="row justify-content-center">
						<div class="col-sm-10 my-4">
							<jdoc:include type="modules" name="content-6" style="none"/>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div id="footer-info">
				<div class="row">	
					<?php if ($this->countModules('footer-1')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="footer-1" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('footer-2')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="footer-2" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('footer-3')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="footer-3" style="none"/>
						</div>
					<?php endif; ?>
					<?php if ($this->countModules('footer-4')) : ?>
						<div class="col my-4">
							<jdoc:include type="modules" name="footer-4" style="none"/>
						</div>	
					<?php endif; ?>	
				</div>	
			</div>	
		<?php } ?>	
	</div>		
	<!-- Footer -->
	<footer class="my-4 text-center">
		<jdoc:include type="modules" name="footage" style="none"/>
	</footer>
	<!-- jQuery -->
	<?php 
		$document->addScript('templates/' . $this->template . '/js/jquery.min.js');
		$document->addScript('templates/' . $this->template . '/js/bootstrap.min.js');
	?>		
</body>
</html>
