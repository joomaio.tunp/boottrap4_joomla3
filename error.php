<?php
	defined('_JEXEC') or die;
	$document = JFactory::getDocument();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content=""> 
	<?php if (!$this->error->getCode()) : ?>
		<jdoc:include type="head" />
	<?php else : ?> 
		<title><?php echo $this->error->getCode() ?> - <?php echo $this->title; ?></title>
	<?php endif; ?>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->baseurl;?>/templates/<?php echo $this->template;?>/css/style_404.css" rel="stylesheet">
</head>
<body>
	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>Oops!</h1>
			</div>
			<h3><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h3>
			<p><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></p>
			<a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>" ><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>
		</div>
	</div>
</body>
</html>
